function validarLogin(){

    console.log($.trim($("#NomeUsuario").val()) == "")
    
    if($.trim($("#NomeUsuario").val()) == ""){
        //alert("Nome incorreto")
        $("#resposta").removeClass("alert-danger alert-dismissible")
        $("#resposta").addClass("alert-danger alert-dismissible")
        $("#resposta").html("Nome incorreto")
    }else{
        if($.trim($("#SenhaUsuario").val()) == ""){
            $("#resposta").removeClass("alert-danger alert-dismissible")
            $("#resposta").addClass("alert-danger alert-dismissible")
            $("#resposta").html("Senha incorreta")
        }else{
            $("#resposta").removeClass("alert-danger alert-dismissible")
            $("#resposta").addClass("alert alert-success")
            $("#resposta").html("Dados inseridos")
            Usuario.login()
        }
    }
}

function validarInformacoesUsuario(operacao){
    //Operacao 1 === cadastrar
    //Operacao 2 === atualizar
    $(".resposta").hide()
    
    
    if($.trim($("#NomeUsuario").val()) == "" || $("#NomeUsuario").val().length > 40){
        $("#respostaUsuario").show()
        $("#respostaUsuario").html("Nome incorreto")
    }else{
        if($.trim($("#SenhaUsuario").val()) == "" || $("#SenhaUsuario") > 10){
            $("#respostaSenha").show()
            $("#respostaSenha").html("Senha incorreta")
        }else{
            if($.trim($("#EmailUsuario").val()) == ""){
                $("#respostaEmail").show()
                $("#respostaEmail").html("E-mail incorreto")
            }else{
                if($.trim($("#NomeCliente").val()) == "" || $("#NomeCliente").val().length > 40){
                    $("#respostaNome").show()
                    $("#respostaNome").html("Nome incorreto")
                }else{
                    if($.trim($("#CpfUsuario").val()) == "" || !validarCpf($("#CpfUsuario").val())){
                        $("#respostaCpf").show()
                        $("#respostaCpf").html("CPF incorreto")
                    }else{
                        if($.trim($("#DataNascimentoCliente").val()) == "" || validarData($("#DataNascimentoCliente").val())){
                            $("#respostaData").show()
                            $("#respostaData").html("Data de nascimento incorreto")
                        }else{
                            if($.trim($("#TelefoneCliente").val()) == ""){
                                $("#respostaTelefone").show()
                                $("#respostaTelefone").html("Telefone incorreto")
                            }else{
                                if($.trim($("#EnderecoCliente").val()) == ""){
                                    $("#respostaEndereco").show()
                                    $("#respostaEndereco").html("Endereço incorreto")
                                }else{
                                    //Inserir ou atualizar
                                    if(operacao == 1){
                                        Usuario.criaUsuario()
                                    }else{
                                        Usuario.atualizar()
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


function validarCpf(cpf){
    if(cpf.trim().length < 11) return false
    if(!vericarSeOsDigitosSaoIguais(cpf)) return false
    return true

}

function vericarSeOsDigitosSaoIguais(cpf){
    let flag
    if( cpf[0] === cpf[1] && cpf[1] === cpf[2] && cpf[2] === cpf[3] &&
        cpf[3] === cpf[4] && cpf[4] === cpf[5] && cpf[6] === cpf[7] &&
        cpf[7] === cpf[8]){
            return false
    }else{
        //Validação do primeiro dígito
        var sequencia = 10, multiplicacao=0;
        for(let i = 0 ; i < 9 ; i++){
            multiplicacao += (sequencia * cpf[i])
            sequencia--
        }
        verificacao1 = (multiplicacao*10) % 11

        if(verificacao1 === 10){
            verificacao1 = 0
        }

        if(verificacao1 !== Number(cpf[9])) return false

        //Validação do segundo dígito
        sequencia = 11, multiplicacao=0;
        for(let i = 0 ; i < 10 ; i++){
            multiplicacao += (sequencia * cpf[i])
            sequencia--
        }

        verificacao1 = (multiplicacao*10) % 11

        if(verificacao1 === 10){
            verificacao1 = 0
        }

        if(verificacao1 !== Number(cpf[10])) return false
        return true

    }
}


function validarData(dado){

    var data = new Date(dado)
    var dataAtual = new Date()

    console.log(data.getFullYear())
    console.log(dataAtual.getFullYear())

    /*
    if(data.getFullYear() < 1900 || 

    //Se tudo for maior
    (data.getDate() > dataAtual.getDate() && data.getMonth() > dataAtual.getMonth() && data.getFullYear() == dataAtual.getFullYear())||

    //Se o dia for maior e o resto igual
    (data.getDate() > dataAtual.getDate() && data.getMonth() == dataAtual.getMonth() && data.getFullYear() == dataAtual.getFullYear())||

    //Se o dia for maior e o resto igual
    (data.getDate() > dataAtual.getDate() && data.getMonth() > dataAtual.getMonth())||
    
    //Se o mês for maior
    (data.getMonth() > dataAtual.getMonth() || data.getFullYear() > dataAtual.getFullYear())||

    //Ano maior que o corrente
    (data.getFullYear() > dataAtual.getFullYear())
    
    ) return true

    return false
    */

    if(
        data.getFullYear() > 1900 &&

        (data.getFullYear() < dataAtual.getFullYear() ||

        (data.getDate() < dataAtual.getDate() && data.getMonth() <= dataAtual.getMonth() && data.getFullYear() == dataAtual.getFullYear()))){
        
        return false
    }
    return true
}

function limpar(){
    $("input").val("")
}

