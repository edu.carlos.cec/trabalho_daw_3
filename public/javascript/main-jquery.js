
Usuario = {
    criaUsuario: () => {
        let t = {}
        t.nome = $("#NomeCliente").val()

        t.cpf = $("#CpfUsuario").val()
        t.telefone = $("#TelefoneCliente").val()
        t.endereco = $("#EnderecoCliente").val()
        t.dt_nascimento = $("#DataNascimentoCliente").val()
        t.usuario = $("#NomeUsuario").val()
        t.senha = $("#SenhaUsuario").val()
        t.email = $("#EmailUsuario").val()

        console.log(t)


        $.ajax({
            type: 'POST',
            url: '/clientes',
            data: t,
            dataType: 'json',
            success: (cliente) => {
                alert(cliente.nome)
            }
        })
    },

    login: () => {
        let t = {}
        t.usuario = $("#NomeUsuario").val()
        console.log(t.usuario)
        t.senha = $("#SenhaUsuario").val()
        console.log(t.senha)

        $.ajax({
            type: 'GET',
            url: '/clientes',
            data: t,
            dataType: 'json',
            success: (data) => {
                console.log(data)
                //Senha do objeto cliente igual a do informado pelo usuário
                if (data.msgCode == 1) {
                    //alert("Deu certo")
                    //Gravar um cookie
                    Cookies.set('usuario', { id: data.id, nome: data.nome })
                    console.log(Cookies.get('usuario'))
                    location.replace("/index")
                } else {
                    alert("Usuário não encontrado")
                }

            }
        })
    },

    preencherCampos: () => {
        let id = {}
        let u = JSON.parse(Cookies.get('usuario'))
        //console.log(u["id"])
        id.idArgumento = u["id"]

        $.ajax({
            type: 'GET',
            url: '/clientes/id',
            data: id,
            dataType: 'json',
            success: (data) => {
                console.log(data)
                efetivarCampos(data)
            }
        })
    },

    atualizar: () => {
        let t = {}
        t.id = JSON.parse(Cookies.get("usuario"))["id"]

        t.nome = $("#NomeCliente").val()
        t.cpf = $("#CpfUsuario").val()
        t.telefone = $("#TelefoneCliente").val()
        t.endereco = $("#EnderecoCliente").val()
        t.dt_nascimento = $("#DataNascimentoCliente").val()
        t.usuario = $("#NomeUsuario").val()
        t.senha = $("#SenhaUsuario").val()
        t.email = $("#EmailUsuario").val()

        $.ajax({
            type: 'PUT',
            url: '/clientes',
            data: t,
            dataType: 'json',
            success: (cliente) => {
                alert("DEU CERTO")
            }
        })
    }
}
var carrinho = []
Pedido = {
    addCarrinho: (id) => {
        carrinho.push(id)

        sessionStorage.setItem('carrinho', JSON.stringify(carrinho))

    }

}


function efetivarCampos(data) {
    $("#NomeUsuario").val(data["usuario"])
    $("#SenhaUsuario").val(data["senha"])
    $("#EmailUsuario").val(data["email"])
    $("#NomeCliente").val(data["nome"])
    $("#CpfUsuario").val(data["cpf"])
    $("#DataNascimentoCliente").val(data["dt_nascimento"])
    $("#EnderecoCliente").val(data["endereco"])
    $("#TelefoneCliente").val(data["telefone"])
}