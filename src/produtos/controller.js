const db = require("./../configs/sequelize")
const Produtos = require("./model")


exports.buscaProdutos = (req, res) => {
    Produtos.findAll().then((prods) => {
        res.send(prods)
    })
}

exports.buscarPorId = (req, res) => {
    Produtos.findOne({
        where: {
            id: req.body.id
        }
    }).then((prod) => {
        res.send(prod)
    })
}