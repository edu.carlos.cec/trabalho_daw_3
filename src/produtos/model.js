const db = require("./../configs/sequelize")
const { Model, DataTypes } = db.Sequelize

const sequelize = db.sequelize

class Produtos extends Model { }
Produtos.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true


    },
    cod: {
        type: DataTypes.STRING
    },
    descricao: {
        type: DataTypes.STRING
    },
    preco: {
        type: DataTypes.DECIMAL
    },
    qtde: {
        type: DataTypes.INTEGER
    }

}, { sequelize, modelName: "produtos" })

module.exports = Produtos