const db = require("./../configs/sequelize")
const { Model, DataTypes } = db.Sequelize

const sequelize = db.sequelize

class Historico extends Model { }
Historico.init({
    id_pedido: {
        type: DataTypes.INTEGER
    },
    id_cliente: {
        type: DataTypes.INTEGER
    }

}, { sequelize, modelName: "historico" })

module.exports = Historico