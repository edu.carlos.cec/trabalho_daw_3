module.exports = (app) => {

    const controller = require('./controller')

    app.post('/clientes', controller.create)

    app.get('/clientes', controller.buscarId)


    app.get('/clientes/id', controller.buscarPorId)


    app.delete('/clientes', controller.remover)

    app.put('/clientes', controller.update)


}