module.exports = (app) => {

    const controller = require('./controller')

    app.post('/pedidos', controller.create)

    app.get('/pedidos/id', controller.ultId)

    app.get('/pedidos', controller.buscaPedidos)


}